package main

import (
	"flag"

	"gitlab.com/onikolas/ned2/editor"
)

func main() {
	settings := flag.String("s", "", "Settings file to use.")
	flag.Parse()
	editor := editor.NewEditor(*settings)
	editor.MainLoop()
}
