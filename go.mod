module gitlab.com/onikolas/ned2

go 1.19

require (
	gitlab.com/onikolas/gapbuffer v0.0.0-20221003215411-1a35e714499d
	gitlab.com/onikolas/math v0.0.0-20220818124502-216a1eb86e2d
	gitlab.com/onikolas/ngl v0.0.0-20221017113646-86877a084996
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/veandco/go-sdl2 v0.4.25 // indirect
	golang.org/x/exp v0.0.0-20221012211006-4de253d81b95 // indirect
	golang.org/x/image v0.0.0-20220902085622-e7cb96979f69 // indirect
)
