package editor

func init() {
	registerCommand(&MoveCursor{})
}

type MoveCursor struct {
	cursorBefore int
}

func (c *MoveCursor) Do(e *Editor, args Args) {
	_, ok := GetArg[bool](args, "lineStart")
	if ok {
		e.CurrentInputWindow.MoveCursorLineStart()
	}
	_, ok = GetArg[bool](args, "lineEnd")
	if ok {
		e.CurrentInputWindow.MoveCursorLineEnd()
	}
	_, ok = GetArg[bool](args, "lineAbove")
	if ok {
		e.CurrentInputWindow.MoveCursorUp()
	}
	_, ok = GetArg[bool](args, "lineBelow")
	if ok {
		e.CurrentInputWindow.MoveCursorDown()
	}
	_, ok = GetArg[bool](args, "fileStart")
	if ok {
		e.CurrentInputWindow.Text.MoveCursor(0)
	}
	_, ok = GetArg[bool](args, "fileEnd")
	if ok {
		e.CurrentInputWindow.Text.MoveCursor(e.CurrentInputWindow.Text.Length())
	}

	d, ok := GetArg[int](args, "distance")
	if !ok {
		return
	}

	c.cursorBefore = e.CurrentInputWindow.Text.Cursor()
	e.CurrentInputWindow.Text.MoveCursor(c.cursorBefore + d)
}

func (c *MoveCursor) Name() string {
	return "MoveCursor"
}
