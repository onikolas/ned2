package editor

var registryIdCounter int = 1
var CommandIdToCommand map[int]Command
var CommandNameToCommandId map[string]int

func registerCommand(c Command) {
	if CommandIdToCommand == nil {
		CommandIdToCommand = map[int]Command{}
		CommandNameToCommandId = map[string]int{}
	}
	CommandIdToCommand[registryIdCounter] = c
	CommandNameToCommandId[c.Name()] = registryIdCounter
	registryIdCounter++
}
