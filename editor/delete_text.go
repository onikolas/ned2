package editor

func init() {
	registerCommand(&DeleteText{})
}

type DeleteText struct {
	text []rune //deleted text (for restoring on undo)
	pos  int    //set to the cursor pos for undoing

}

// Deletes text at a given position.
func (c *DeleteText) Do(e *Editor, args Args) {
	num, ok := GetArg[int](args, "num")
	if !ok {
		return
	}
	if num > 0 {
		e.CurrentInputWindow.Text.DeleteForward(uint(num))
	} else {
		e.CurrentInputWindow.Text.DeleteBack(uint(-num))
	}
}

func (c *DeleteText) Name() string {
	return "DeleteText"
}
