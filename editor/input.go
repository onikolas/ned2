package editor

import (
	"fmt"
	"strings"

	"gitlab.com/onikolas/ngl/input"
)

// Binds user specified commands to editor commands
type CommandBind struct {
	// id for this bind - same command can be binded multiple times, possibly with different inputs
	BindId  int
	Command CommandInvocation
}

// Handles user inputs
type Input struct {
	inputParser  *input.InputParser
	commandBinds map[int]CommandBind
	abbr         map[string]input.InputEventMatcher
}

func (i *Input) Setup(s *Settings) {
	i.abbreviate()
	i.inputParser = &input.InputParser{}
	i.inputParser.EnableTextParsing()
	i.commandBinds = map[int]CommandBind{}

	bindId := 1 // zero is taken as ActionNone
	for keybind, command := range s.Keybinds {
		stroke := []*input.ParseTreeNode{}

		tokens := strings.Split(string(keybind), " ")
		// each token must correspond to an input event
		for _, tok := range tokens {
			if len(tok) > 1 {
				inputEvent, ok := i.abbr[tok]
				if !ok {
					panic(fmt.Sprint("Could not parse token ", tok, " in keybinds"))
				}
				stroke = append(stroke, &input.ParseTreeNode{Input: inputEvent})
			} else {
				stroke = append(stroke, &input.ParseTreeNode{Input: input.NewKeyBoardEventText(tok)})
			}
		}

		// last node gets attached the command
		commandId, ok := CommandNameToCommandId[command.Command]
		if !ok {
			panic(fmt.Sprint("Command ", command.Command, " does not exist"))
		}
		fmt.Println("Binding command ", command, "with id ", commandId, " to keypress ", stroke)
		cmdBind := CommandBind{
			BindId:  bindId,
			Command: command,
		}
		cmdBind.Command.CommandId = commandId //not in user settings so need to add manually
		i.commandBinds[bindId] = cmdBind
		stroke[len(stroke)-1].Action = input.InputAction(bindId)

		if err := i.inputParser.AddSequence(stroke); err != nil {
			panic(err)
		}
		bindId++
	}
}

func (i *Input) GetBoundCommand(action int) (CommandBind, bool) {
	if cb, ok := i.commandBinds[action]; ok {
		return cb, true
	}
	return CommandBind{}, false
}

// If the users's inputs match a bound command return it along with any static parameters set in user
// settings. The inputs that led to the command are also passed as Args (with id:parserInputs)
func (i *Input) Parse(in input.InputEventMatcher) (CommandInvocation, bool) {
	i.inputParser.BufferInput(in)
	match, action, parsedInputs := i.inputParser.Parse()
	if match == input.MatchTypeFull {
		commandInvocation := NewCommandInvocation()
		commandInvocation.Args["parserInputs"] = parsedInputs

		// special treatment for text due to the parser returning a reserved action for it that
		if action == input.InputActionText {
			fmt.Println("parsed text input")
			commandInvocation.Command = "InsertTExt"
			commandInvocation.CommandId = CommandNameToCommandId["InsertText"]
			return commandInvocation, true
		}
		// other commands should be bound in Input.Setup
		if commandBind, ok := i.GetBoundCommand(int(action)); ok {
			fmt.Println("parsed:", commandBind)
			commandInvocation = commandBind.Command
			commandInvocation.Args["parserInputs"] = parsedInputs
			return commandInvocation, true
		} else {
			fmt.Println("action ", action, " not bound in Input")
			return CommandInvocation{}, false
		}

	}
	return CommandInvocation{}, false
}

// this can be part of settings
func (i *Input) abbreviate() {
	i.abbr = map[string]input.InputEventMatcher{}

	i.abbr["ctrl-d"] = input.NewKeyBoardEvent(input.KeyControlDown)
	i.abbr["ctrl-u"] = input.NewKeyBoardEvent(input.KeyControlUp)
	i.abbr["del"] = input.NewKeyBoardEvent(input.KeyDelete)
	i.abbr["bkspc"] = input.NewKeyBoardEvent(input.KeyBackspace)
	i.abbr["right"] = input.NewKeyBoardEvent(input.KeyRight)
	i.abbr["left"] = input.NewKeyBoardEvent(input.KeyLeft)
	i.abbr["up"] = input.NewKeyBoardEvent(input.KeyUp)
	i.abbr["down"] = input.NewKeyBoardEvent(input.KeyDown)
}
