package editor

// Command is wrapper for editor functions.
type Command interface {
	Do(e *Editor, args Args)
	Name() string
	// Usage()string // describes how to call this Command - useful in user bindings
}

// Used when calling a command.
type CommandInvocation struct {
	Command   string
	CommandId int
	Args      Args
}

func NewCommandInvocation() CommandInvocation {
	return CommandInvocation{
		Args: NewArgs(),
	}
}

type Args map[string]any

func NewArgs() Args {
	return make(Args)
}

// Get an argument of type T named name
func GetArg[T any](a Args, name string) (T, bool) {
	var arg T
	t, ok := a[name]
	if !ok {
		return arg, false
	}
	arg, ok = t.(T)
	if !ok {
		return arg, false
	}
	return arg, true
}

// A command that can be undone. Undoable commands must be queued in the editor's undo stack when executed.
type Undoable interface {
	Undo()
}
