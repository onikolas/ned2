package editor

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v3"
)

// Settings holds user settings read from a configuration file.
type Settings struct {
	Keybinds map[string]CommandInvocation
}

func (s *Settings) FromFile(filename string) error {
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	if err = yaml.Unmarshal(bytes, s); err != nil {
		return err
	}
	fmt.Println("Read settings file ", filename)
	fmt.Println(s)
	return nil
}
