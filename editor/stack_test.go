package editor

import "testing"

func TestStack(t *testing.T) {
	s := Stack{}

	pushes := []interface{}{0, 10, "asd"}
	pops := []interface{}{"asd", 10, 0}

	for _, v := range pushes {
		s.Push(v)
	}
	for _, v := range pops {
		if res, err := s.Pop(); err != nil || res != v {
			t.Error(res, err)
		}
	}

	// empty case
	if res, err := s.Pop(); err == nil || res != nil {
		t.Error(res, err)
	}
}
