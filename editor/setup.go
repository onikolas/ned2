package editor

import (
	"gitlab.com/onikolas/math"
	"gitlab.com/onikolas/ngl/platform"
	"gitlab.com/onikolas/ngl/shaders"
	"gitlab.com/onikolas/ngl/sprite"
	"gitlab.com/onikolas/ngl/text"
	"gitlab.com/onikolas/ngl/ui"
)

var (
	renderOrderBorder    int = 1
	renderOrderAnimation     = 2
	renderOrderText          = 3
)

var Stores map[string]*sprite.SpriteStore
var GlbFont *text.Font

func setupRendering() *sprite.Renderer {

	if err := platform.InitializeWindow(1000, 1000, "NED2", true); err != nil {
		panic(err)
	}

	renderer := sprite.Renderer{}
	renderer.Init()

	Stores = make(map[string]*sprite.SpriteStore)

	// default shader
	shader, err := shaders.NewColorSpriteShader()
	if err != nil {
		panic(err)
	}

	// atlas for drawing borders
	borderStore, err := ui.NewBorderStore("assets/border3.png", shader, renderOrderBorder)
	if err != nil {
		panic(err)
	}

	// use an animated sprite as cursor
	cursorStore, err := sprite.NewSpriteStoreFromFiles("assets/pumpkin_dude.png",
		"assets/pumpkin_dude.atlas", shader, 2, renderOrderAnimation)
	if err != nil {
		panic(err)
	}

	renderer.AddSpriteStore(borderStore)
	renderer.AddSpriteStore(cursorStore)
	Stores["border"] = borderStore
	Stores["cursor"] = cursorStore

	// text rendering
	textShader, err := text.NewTextShader()
	if err != nil {
		panic(err)
	}
	textStore, font, err := text.CreateTextStore("assets/font.ttf", textShader, text.CharacterSetASCII(), 32, renderOrderText)
	GlbFont = font
	renderer.AddSpriteStore(textStore)
	Stores["text"] = textStore

	return &renderer
}

func setupUI() *ui.MainWindow {
	mainWindow := &ui.MainWindow{}
	mainWindow.UI.BoundingBox.P2 = math.Vector2[int]{X: 1000, Y: 1000}
	win := NewTextEditWindow()
	mainWindow.AddChild(win)
	return mainWindow
}
