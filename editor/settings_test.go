package editor

import (
	"fmt"
	"testing"

	"gopkg.in/yaml.v3"
)

func TestExportSettings(t *testing.T) {
	s := Settings{
		map[string]CommandInvocation{
			"c-c c-v": {
				Command: "Copy",
				Args: map[string]any{
					"arg1":    12,
					"somearg": "arst",
				},
			},
		},
	}

	bytes, _ := yaml.Marshal(s)
	fmt.Println(string(bytes))
}
