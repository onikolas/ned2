package editor

import (
	"fmt"
	"time"

	"gitlab.com/onikolas/ngl/platform"
	"gitlab.com/onikolas/ngl/sprite"
	"gitlab.com/onikolas/ngl/ui"
)

// Editor holds all data needed for Commands
type Editor struct {
	MainWindow         *ui.MainWindow
	Events             platform.EventManager
	CurrentInputWindow *ui.TextInput
	UndoStack          Stack
	Input              Input

	renderer *sprite.Renderer
	settings Settings
}

func NewEditor(settingsFile string) *Editor {
	e := &Editor{}
	e.Init(settingsFile)
	return e
}

func (e *Editor) Init(settingsFile string) {
	if err := e.settings.FromFile(settingsFile); err != nil {
		panic(err)
	}

	e.renderer = setupRendering()
	e.MainWindow = setupUI()
	e.Events = platform.EventManager{}
	e.Input.Setup(&e.settings)

	ok := false
	e.CurrentInputWindow, ok = ui.FindByType[*ui.TextInput](e.MainWindow)
	if !ok {
		panic("yikes")
	}
}

func (e *Editor) MainLoop() {
	frameDt := time.Duration(0)

	for {
		frameStartTime := time.Now()

		e.Events.GatherEvents()
		if e.Events.GetQuitEvent() {
			return
		}

		for _, v := range e.Events.GetResizeEvents() {
			e.renderer.Resize(v.X, v.Y)
			e.MainWindow.UI.BoundingBox.P2 = v
		}

		for _, v := range e.Events.GetKeyboardEvents() {
			if commandInvocation, parseMatch := e.Input.Parse(&v); parseMatch {
				if command, ok := CommandIdToCommand[commandInvocation.CommandId]; ok {
					fmt.Println("Do-ing: ", command)
					command.Do(e, commandInvocation.Args)
				} else {
					fmt.Println("Attempted to execute command that is not registered")
				}
			}
		}

		e.MainWindow.Update(0, frameDt)
		ui.Draw(e.MainWindow)
		e.renderer.Render()
		frameDt = time.Since(frameStartTime)
	}
}
