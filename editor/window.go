package editor

import (
	"gitlab.com/onikolas/ngl/ui"
)

// Window is a sub-area within the editor that contains a ui element. It's decorated with a border.
type Window struct {
	border  *ui.Border
	element ui.Element
}

func NewTextEditWindow() *ui.Border {
	border, err := ui.NewBorderDefault(Stores["border"], 10)
	animatedCursor := ui.NewSprite(Stores["cursor"].AtlasMap.Named["run"], Stores["cursor"], 10)
	if err != nil {
		panic(err)
	}
	txt := ui.NewTextInput("Text!", GlbFont, Stores["text"], animatedCursor)
	border.AddChild(txt)
	return border
}
