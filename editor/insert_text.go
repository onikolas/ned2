package editor

import "gitlab.com/onikolas/ngl/input"

func init() {
	registerCommand(&InsertText{})
}

type InsertText struct {
	text []rune //text to add
	pos  int    //set to the cursor pos for undoing
}

func (c *InsertText) Do(e *Editor, args Args) {
	text := ""
	pos, ok := GetArg[int](args, "pos")
	if !ok {
		pos = e.CurrentInputWindow.Text.Cursor()
	}
	text, ok = GetArg[string](args, "text")

	// no text passed so look for keyboard input
	if keyboard, okk := GetArg[[]input.InputEventMatcher](args, "parserInputs"); !ok && okk {
		if key, ok := keyboard[0].(*input.KeyboardEvent); ok {
			text = key.Text
		}
	}

	if text == "" {
		return
	}

	c.text = []rune(text)
	c.pos = pos
	e.CurrentInputWindow.Text.Insert(c.text, pos)
}

func (c *InsertText) Name() string {
	return "InsertText"
}
